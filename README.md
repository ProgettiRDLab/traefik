#lancio traefik
docker-compose -f traefik-service.yml up -d

#lancio il primo wordpress sulla NETWORK creata di default da traefik
docker-compose -f wordpress.yml up -d

#lancio il secondo wordpress sulla NETWORK creata di default da traefik
docker-compose -f wordpressdue.yml up -d


#altre info
docker swarm
docker network ls
docker stack ls
docker node ls
docker ps